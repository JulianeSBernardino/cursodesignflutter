import 'package:flutter/material.dart';

class AnimacaoImplicita extends StatefulWidget {
  @override
  _AnimacaoImplicitaState createState() => _AnimacaoImplicitaState();
}

class _AnimacaoImplicitaState extends State<AnimacaoImplicita> {
  bool isLoading = false;
  _onTap(){
    setState(() {
      isLoading = !isLoading;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Animation'),),
      body: Center(
        child: GestureDetector(
          onTap: _onTap,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 800),
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.circular(isLoading ? 40 : 10)
          ),
          width: isLoading ? 50 : 250, 
          height: 50,
          alignment: Alignment.center,
          child: isLoading 
          ? Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          )
          : Text('Botao', textAlign: TextAlign.center,),
        ),
        )
      ),
    );
  }
}