import 'package:flutter/material.dart';

class RecriandoRaisedButton extends StatefulWidget {
  @override
  _RecriandoRaisedButtonState createState() => _RecriandoRaisedButtonState();
}

class _RecriandoRaisedButtonState extends State<RecriandoRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recriando RaisedButton'),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: (){
                print('Clicou');
              },
              color: Colors.amber,
              child: Text('Click button'),
            ),
            RaisedButton.icon(
              onPressed: (){
                print('RaisedButton.icon');
              }, 
              icon: Icon(Icons.android),
              label: Text('Click button'),
            )
          ],
        ),
      )
      
    );
  }
}