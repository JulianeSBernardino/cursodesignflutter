import 'package:cursodesign/conteudos/animacoes/animacaoControlada.dart';
import 'package:cursodesign/conteudos/animacoes/animacaoImplicita.dart';
import 'package:cursodesign/conteudos/raisedButton.dart';
import 'package:cursodesign/conteudos/telausuario.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TelaUsuario(),
      // initialRoute: '/',
      // routes: {
      //   '/': (context) => RecriandoRaisedButton(),
      // },
    );
  }
}

