import 'package:flutter/material.dart';

class TelaUsuario extends StatefulWidget {
  @override
  _TelaUsuarioState createState() => _TelaUsuarioState();
}

class _TelaUsuarioState extends State<TelaUsuario> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Conta de Usuário'),
      ),
      body: Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: ClipOval(
              child: Image.network(
                "https://yt3.ggpht.com/a-/AN66SAx3MtGREYEACvCNPELeeqxtmYg32rD4FCUcJg=s900-mo-c-c0xffffffff-rj-k-no",
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            "Jacob A. Moura",
            style: TextStyle(
                color: Colors.grey[700],
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
          Text(
            "jacobaraujo7@gmail.com",
            style: TextStyle(color: Colors.grey, fontSize: 18),
          ),
          SizedBox(
            height: 30,
          ),
          Material(
            elevation: 7,
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(40),
            child: InkWell(
              borderRadius: BorderRadius.circular(40),
              onTap: () {
                print("Clicou!");
              },
              child: Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(40)),
                padding: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                child: Text("SAIR", style: TextStyle(color: Colors.white)),
              ),
            ),
          )
        ],
      ),
    ),
    
    );
  }
}